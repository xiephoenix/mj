<?php
class ElephantController extends BaseController
{	
	const MAX_POS=4;
	/**
	 * 初始化登录用户 {"c":"Elephant","m":"index"}
	 */
	private function init()
	{
		$this->mUser = new UserModel();
	}
	public function indexAction(){
		$this->success('nothing');
	}
	public function getGame(){
		$user = $this->mUser->getInfo();
		if(empty($user['roomid'])){
			$this->soerror('没有找到房间');
			return false;
		}
		return new ElephantajiangModel($this->mUser, $user['roomid']);
	}
	public  function messageAction(){
		$game = $this->getGame();
		$user = $this->mUser->getInfo();
		$res =$game->message($user);
		if(!$res){
			$this->error($game->getError());
			return;
		}
		$this->success($res);
	}
	public function fightAction(){
		$game = $this->getGame();
		$res =$game->fight();
		if(!$res){
			$this->error($game->getError());
			return;
		}
		$this->success($res);
	}
	/**
	 * 踢人
	 * [kickAction description]
	 * @return [type] [description]
	 */
	public function kickAction(){
		$game = $this->getGame();
		$res =$game->kick();
		if(!$res){
			$this->error($game->getError());
			return;
		}
		$this->success($res);
	}
	/**
	 * 点击头像的信息
	 * [informationAction description]
	 * @return [type] [description]
	 */
	public function informationAction(){
		$data = $this->getRequest()->getParams();		
		$seatid = $data['seatid'];//表情符号
		$game = $this->getGame();
		$res =$game->information($seatid);
		if(!$res){
			$this->error($game->getError());
			return;
		}
		$this->success($res);
	}
	public function offlineAction(){}
	//获取房间信息
	public function infoAction()
	{
		$game = $this->getGame();
		$room = $game->getInfoEx();
		$this->success($room);
	}
	/**
	 * 获取房主信息
	 * [reconnetAction description]
	 * @return [type] [description]
	 */
	public function reconnetAction(){
		$game = $this->getGame();
		$response = $game->reconnet();
		
		if(!$response){
			$this->error($game->getError());
		}else{
			$this->success($game->_success);
		}
	}
	public function expressionAction(){
		$data = $this->getRequest()->getParams();	
		$emoji = $data['emoji'];//表情符号
		$type = $data['type'];//self  other
		$money = $data['money'];//多少钻石
		$money = intval($money);
		if($type=="other" && $money){
			//判断是否够钻石
			
			$user = $this->mUser->getInfo();

			$gold =$user['gold'];
			if($gold<$money){
				$this->soerror('钻石不足');
				return;			
			}
		}
		$receive_seatid =$data['seatid'];

		$game = $this->getGame();
		$res =$game->expression($emoji,$type,$receive_seatid,$money);
		if(!$res){
			$this->error($game->getError());
			return;
		}
	}
	//站起来
	public function standupAction(){
		$data = $this->getRequest()->getParams();		
		$seatid = intval($data['seatid']);
		if( $seatid>4 ||$seatid<=0){
			$this->soerror('无效座位号'.$seatid);
			return;
		}
		$game = $this->getGame();
		$res =$game->standup($seatid);
		if(!$res){
			$this->error($game->getError());
			return;
		}
		$this->success(array('message'=>'站起成功'));
	}
	/**
	 * 玩家准备
	 * [readyAction description]
	 * @return [type] [description]
	 */
	public function readyAction(){
		$game = $this->getGame();
		$res =$game->ready();
		if(!$res){
			$this->error($game->getError());
			return;
		}
		//$this->success(array('tips'=>'准备成功'));
	}
	//坐下来 占位置
	public function seatAction(){
		$data = $this->getRequest()->getParams();		
		$seatid = intval($data['seatid']);
		$user = $this->mUser->getInfo();
		if($user['gold'] <100){
			$this->soerror('钻石不足');
			return ;
		}
		if( $seatid>4 ||$seatid<=0){
			$this->soerror('无效座位号'.$seatid);
			return;
		}
		$game = $this->getGame();
		$res = $game->seat($seatid);
		if(!$res){
			$this->error($game->getError());
			return;
		}
		//$this->success($res);
	}
	//加入房间
	public function joinAction(){
		$data = $this->getRequest()->getParams();
		$roomid = $data['roomid'];
		if(!$roomid){
			$this->soerror('房间号不存在.');
			return ;
		}
		$mUser = new UserModel();
		$user = $mUser->getInfo();
		//是否够钻石
		if($user['gold']<100){
			$this->soerror($user['gold']);
			return ;
		}
		$this->redis = RedisDB::factory('game');
		//判断是否已经过期 房间号
		$roomInfo = $this->redis->hGetAll(ElephantajiangModel::INFO_KEY . $roomid);
		$created =$roomInfo['time'];//创建时间
		$time_value =$roomInfo['time_value'];
		$time_way =$roomInfo['time_way'];
		$end_time = $created+$time_value*60;

		if(!$roomInfo || time()> $end_time){
			$tips=array('status'=>2,'roomid'=>$roomid,'tips'=>$roomid.'已经解散了.');

			$this->error($tips);
			return ;
		}
		if($user['roomid']){
			$roomInfo = $this->redis->hGetAll(ElephantajiangModel::INFO_KEY . $user['roomid']);
			//游戏进行中
			if($roomInfo && $roomInfo['state']==1 && $roomInfo['pan']>=1){
				$tips=array('status'=>1,'roomid'=>$user['roomid'],'tips'=>"已经在房间,房间号:".$user['roomid']);
				$this->error($tips);
				return ;
			}
		}
		$mGame = new ElephantajiangModel($this->mUser, $roomid);
		//$user = $this->mUser->getInfo();
		$ret = $mGame->join();
		if(!$ret){
			$this->error($mGame->getError());
			return ;
		}
		//$this->success($ret);
	}
	/**
	 * 摸牌阶段
	 * [touchcardAction description]
	 * @return [type] [description]
	 */
	public function touchcardAction(){
		$game = $this->getGame();
		$response = $game->touchcard();
		if(!$response){
			$this->error($game->getError());
		}else{
			$this->success($game->_success);
		}
	}
	public function refreshAction(){

		$game = $this->getGame();
		$response = $game->refresh();
		if(!$response){
			$this->error($game->getError());
		}else{
			$this->success($game->_success);
		}
	}

	//出牌阶段
	public function shotsAction(){
		$data = $this->getRequest()->getParams();
		$throwpai = $data['throwpai'];
		$throwType =$data['throwType'];//0- 普通出牌 1- 杠 2-胡
		$throwpai =intval($throwpai);
		if(!$throwpai){
			$this->soerror('无效牌');
			return ;
		}
		$game = $this->getGame();
		$response = $game->shots($throwpai,$throwType);
		if(!$response){
			$this->error($game->getError());
		}else{

			//$this->success($game->_success);
		}
	}
	public function overAction(){
		$game = $this->getGame();
		$response = $game->over();
		if(!$response){
			$this->error($game->getError());
		}else{

			$this->success($game->_success);
		}
	}
	//吃 碰 杠 胡
	public function checkKindAction(){
		$data = $this->getRequest()->getParams();
		$pai = $data['pai'];//吃 碰 杠 胡的牌
		$type = $data['type'];// 1-吃 2-碰 3-杠 4 胡 5过
		//$throw_seatid =$data['throw_seatid'];
		$game = $this->getGame();
		$response = $game->dealPai($pai,$type);
		if(!$response){
			$this->error($game->getError());
		}else{

			//$this->success($game->_success);
		}
	}
	//开始
	public function startAction()
	{		
		$game = $this->getGame();
		$response = $game->start();
		if(!$response){
			$this->error($game->getError());
		}else{
			$this->success($game->_success);
		}
	}

	//退出房间
	public function leaveAction()
	{
		$mUser = new UserModel();
		$user = $mUser->getInfo();
		if(!$user['roomid']){
			$this->soerror('NotInRoom');
			return ;
		}
		$mGame = new ElephantajiangModel($mUser, $user['roomid']);
		$ret = $mGame->leave();
		if(!$ret){
			$this->error('LeaveFailed');
			return ;
		}
		$this->success(array('message'=>'退出房间'.$user['roomid']));
	}
}