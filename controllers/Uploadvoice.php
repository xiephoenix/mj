<?php

use Thenbsp\Wechat\OAuth\Client;
use Thenbsp\Wechat\Message\Entity\Voice;
use Thenbsp\Wechat\Wechat\AccessToken;
class UploadvoiceController extends WechatController {

	private $user;
	public function init()
	{	
		parent::init();
		$this->mUser = new UserModel(null,false);
		$this->mUser->getUidByFromuser($this->fromuser);
		$this->user = $this->mUser->getInfo();
	}
	public function getGame(){
		
		if(empty($user['roomid'])){
			throw new Exception('不在房间里面.');
		}

		return new ElephantajiangModel($this->mUser, $this->user['roomid']);
	}
	public function tAction(){
		
	}
	public function indexAction(){
		$config_obj=Yaf_Registry::get("config");
		$config_wechat=$config_obj->wechat->toArray();
		$url = $config_wechat['uploadfile'];
		$appid =$config_wechat['appid'];
		$appsecret=$config_wechat['appsecret'];
		$class_accessToken = new AccessToken($appid, $appsecret);		
		$this->redis = RedisDB::factory('wechat');

		$cacheDriver = new \Doctrine\Common\Cache\RedisCache();
		$cacheDriver->setRedis($this->redis);

		$class_accessToken->setCache($cacheDriver);
		$class_accessToken->clearFromCache();
		$access_token = $class_accessToken->getTokenString();
		$userInfo = $this->user;
		$seatid =0;
		$roomid =0;
		if(is_array($userInfo)&&$userInfo){
			$seatid =$userInfo['seatid'];
			$roomid =$userInfo['roomid'];
			$uid =$userInfo['uid'];
		}
		if(!$seatid ||!$roomid){
			$this->response(401, '没有加入房间',null);
		}
		$media_id =$_POST['media_id']; //$this->getRequest()->getQuery('','6AJLCDFGad9nZoZClCZ6L9XrXQX_GOpUFQEx-C6sb34fuNaKR-8RZ0X3A-YG3uV1');
		if(!$media_id) $media_id='6AJLCDFGad9nZoZClCZ6L9XrXQX_GOpUFQEx-C6sb34fuNaKR-8RZ0X3A-YG3uV1';
		$url.="access_token=".$access_token."&media_id=".$media_id;
		$return_content = $this->http_get_data($url);
		$body = json_decode($return_content);
		if($body->errcode){
			$this->response(402, $body->errmsg,null);
		}
		$pathName=date('Y-m-d');
        $this->checkUploadPath('audio'.'/'.$pathName);
		$prefix=md5(uniqid(microtime(true),true));
		$filename = $prefix.".amr";
		$mp3='audio'.'/'.$pathName.'/'.$prefix.".mp3";
		$fp= @fopen("audio/".$pathName.'/'.$filename,"a"); //将文件绑定到流
		
		$fw=fwrite($fp,$return_content); //写入文件 
		fclose($fp);  //关闭文件流
		if($fw){
			$rp="audio/".$pathName.'/'.$filename;
			if(file_exists($mp3)){			
				$this->response(401,'失败',null);
			}else{
				$command = "/usr/local/bin/ffmpeg -i $rp $mp3";  
				system($command,$error);  
			}
			$config_obj=Yaf_Registry::get("config");
			$config_wechat=$config_obj->wechat->toArray();
			$config_database =$config_obj->database->config->toArray();
			
			//保存进入数据库
			$save_data =array(
				'created'=>time(),
				'path'=>'/'.$mp3,
				'media_id'=>$media_id,
				'roomid'=>$roomid,
				'seatid'=>$seatid,
				'uid'=>$uid,
			);
			$url ="http://".$_SERVER['HTTP_HOST']."/".$mp3;
			$prefix =$config_database['prefix'];
			$mediaTable = new HbModel($prefix.'medias');
			$mediaTable->add($save_data);
			
			Socket::sendToChannel("game-{$roomid}", "Pushment", "sendvoice", array(
				'seatid' => $seatid,
				'voice_url'=>$url,
			));
			//$this->response(200,'成功',null);
			
		}else{
			$this->response(401,'失败',null);
		}
	}
	private  function checkUploadPath($uploadPath){
		if(!file_exists($uploadPath)){
			mkdir($uploadPath,0777,true);
		}
	}
}