<?php
class PlayController extends WechatController {

	private $user;
	const LIST_KEY = 'roomlist';
	const INFO_KEY = 'room:';
	public function init()
	{	
		parent::init();
		$this->mUser = new UserModel(null,false);
		$this->mUser->getUidByFromuser($this->fromuser);
		$this->user = $this->mUser->getInfo();
	}
	public function indexAction(){
		$url ="http://".$_SERVER['SERVER_NAME']."/mahjong/#!/main";
		header("location:".$url);
	}
	public function chiAction(){
		$roomlist = $this->redis->hGetAll(self::LIST_KEY);
		$mUser = new UserModel(1);
		$mGame = new ElephantajiangModel($mUser, key($roomlist));
		//$handscards = array(2,5,5,7,22,22,26,27,27,35,35,12,13,14);
		//$handscards = array(8,8,8,17,17,31,31,31,14,14,14,13,14,15);
		//$handscards =$mGame->sortBy($handscards);
		//echo $mGame->checkHu($handscards);
		$mGame->getFindEat();
	}
}