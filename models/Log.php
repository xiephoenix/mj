<?php
class LogModel extends BaseModel
{
	const LIST_KEY = 'log';
	private $uid	= null;
	private $roomid =null;
	public function __construct($roomid,$uid = null)
	{	
		$this->uid = $uid;
		$this->roomid =$roomid;
		$this->redis = RedisDB::factory('log');
	}
	/**
	 * 保存用户扣钻石记录日志
	 * [save description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function save($data)
	{
		$this->redis->hMSet(self::LIST_KEY . $this->roomid, $data);
	}
	/**
	 *  房间解散 返还100钻石
	 * [getUsers description]
	 * @return [type] [description]
	 */
	public function getUsers(){
		$uidlist = $this->redis->hGetAll(self::LIST_KEY . $this->roomid);
		if(is_array($uidlist) && $uidlist){
			foreach ($uidlist  as $key => $value) {
				$now_uid =$value;
				$mUser=new UserModel($now_uid);
				$user =$mUser->getInfo();
				//返还100
				$user['gold']+=100;
				$user['roomid']="";
				$user['seatid']="";
				$mUser->save($user);
				//加入消息记录
				$msg=new MsgModel($this->roomid);
				$new_msg[] =array(
					'type'=>'system',
					'uid'=>$now_uid,
					'is_send'=>1,
					'nickname'=>$user['nickname'],
					'headimgurl'=>$user['headimgurl'],
					'msg'=>'因房间*'.$this->roomid.'*未开局,系统已返还100个钻石',
				);
				$message =array('data'=>json_encode($new_msg));
				$msg->bsave($now_uid,$message);
			}
		}
	}
	public function getUidByRoom(){
		$uidlist = $this->redis->hGetAll(self::LIST_KEY . $this->roomid);
		return $uidlist;
	}
	public function del(){
		$uidlist = $this->redis->hGetAll(self::LIST_KEY . $this->roomid);
		$this->redis->del(self::LIST_KEY . $this->roomid);

	}
	public function isExist(){
		$uidlist = $this->redis->hGetAll(self::LIST_KEY . $this->roomid);
		$is_seat=false;
		if(is_array($uidlist) && $uidlist){
			foreach ($uidlist  as $key => $value) {
				if($value==$this->uid) {
					$is_seat=true;
					break;
				}
			}
		}
		return $is_seat;
	}
}