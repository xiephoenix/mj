<?php
class MsgModel extends BaseModel
{
	const LIST_MSG='msglist';
	const LIST_INFO ='info:';
	private $roomid	= null;
	public function __construct($roomid)
	{
		parent::__construct();
		
		$this->redis = RedisDB::factory('message');
		$this->roomid = $roomid;

	}
	public function save($data)
	{
		$this->redis->hMSet(self::LIST_MSG . $this->roomid, $data);
	}
	/**
	 * 保存系统消息
	 * [bsave description]
	 * @param  [type] $uid  [description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function bsave($uid,$data)
	{
		$this->redis->hMSet(self::LIST_INFO . $uid, $data);
	}
	/**
	 * 全是未读消息
	 * [getMsg description]
	 * @param  [type] $user [description]
	 * @return [type]       [description]
	 */
	public function getMsg($user){
		if(!$user) return;
		$uid =$user['uid'];
		$msg = $this->redis->hGetAll(self::LIST_INFO . $uid);
		
		//system消息 设置读为2
		$new_msg=array();
		if(isset($msg['data'])&&$msg['data']){
			$out =json_decode($msg['data'],true);
			foreach ($out as $key => $value) {
				if($value['is_send']==1){
					$new_msg[]=$value;
				}
			}
		}
		return $new_msg;
	}
	public function delete(){
		$this->redis->del(self::LIST_MSG . $this->roomid);
	}
}