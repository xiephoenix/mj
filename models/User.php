<?php
class UserModel extends BaseModel
{
	const INFO_KEY = 'user:';
	const AUTH_KEY = 'auth';
	const USERNAME_KEY = 'username_index';
	const USERID_KEY = 'userid';
	const LIST_USER='userlist';
	private $uid	= null;
	private $userinfo = null;
	
	public function __construct($uid = null, $load = true)
	{
		parent::__construct();
		
		$this->redis = RedisDB::factory('user');
		
		if($uid){
			//指定uid
			$this->uid = $uid;
		}else if($load){
			$this->uid = Socket::getUid(SESSIONID);
			if(!$this->uid){
				throw new Exception('Unauth');
			}
		}
	}
	public function getUidByFromuser($fromuser){
		if(!$fromuser) return;
		$uid =0;
		$userlist = $this->redis->hGetAll(self::LIST_USER);
		if(is_array($userlist) && $userlist){
			foreach($userlist as $cur_uid=>$user){
				$userInfo =json_decode($user,true);
				if($userInfo['username']==$fromuser){
					$uid =$cur_uid;
					break;
				}
			}
		}
		$this->uid =$uid;
		return $uid;
	}
	public function getUid()
	{
		return $this->uid;
	}
	public  function getUserInfoByUid()
	{
		$userinfo = $this->redis->hGetAll(self::INFO_KEY . $this->uid);
		return $userinfo;
	}
	public function getInfo()
	{
		if($this->userinfo === null){
			$this->userinfo = $this->redis->hGetAll(self::INFO_KEY . $this->uid);
		}
		
		return $this->userinfo;
	}
	
	public function save($data)
	{	
		if(!is_array($data)) return;
		$this->redis->hMSet(self::INFO_KEY . $this->uid, $data);
	}
	
	public function incr($key, $value)
	{
		$this->redis->hIncrBy(self::INFO_KEY . $this->uid, $key, $value);
	}
	
	public function login($username, $token)
	{
		$val = $this->redis->hGet(self::AUTH_KEY, $username);
		if($val != $token){
			//return false;
		}
		
		//remove auth info
		$this->redis->hDel(self::AUTH_KEY, $username);
		
		//get userid
		$this->uid = $this->redis->hGet(self::USERNAME_KEY, $username);
		if(!$this->uid){
			return false;
		}
		
		return true;
	}
}