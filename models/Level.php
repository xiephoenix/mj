<?php
class LevelModel extends BaseModel
{
	private $uid	= null;
	public function __construct($uid = null)
	{	
		$this->uid = $uid;
	}
	public function getLevel(){
		$config_obj=Yaf_Registry::get("config");
		$config_database =$config_obj->database->config->toArray();
		$prefix =$config_database['prefix'];
		$orderTable =new HbModel($prefix.'order');
		$sql="SELECT sum(amount) as total  FROM gm_order WHERE uid=".$this->uid." AND is_pay=2";
		$res=$orderTable->query($sql);
		$pay_amount=0;
		if(isset($res[0]['total']) && $res[0]['total']){
			$pay_amount =$res[0]['total'];
		}
		$level_permit =array(
				10=>array(
					'title'=>'VIP1',
					'discount'=>0.98,
				),
				50=>array(
					'title'=>'VIP2',
					'discount'=>0.97,
				),
				100=>array(
					'title'=>'VIP3',
					'discount'=>0.96,
				),
				200=>array(
					'title'=>'VIP4',
					'discount'=>0.95,
				),
				400=>array(
					'title'=>'VIP5',
					'discount'=>0.94,
				),
				700=>array(
					'title'=>'VIP6',
					'discount'=>0.93,
				),
				1100=>array(
					'title'=>'VIP7',
					'discount'=>0.92,
				),
				1600=>array(
					'title'=>'VIP8',
					'discount'=>0.9,
				),
				2200=>array(
					'title'=>'VIP9',
					'discount'=>0.88,
				),
			);
		$title ="VIP0";
		$discount=1;
		if($pay_amount){
			$prev_key=0;
			foreach($level_permit as $key=>$val){
				if($key<=$pay_amount){
					$prev_key =$key;
					$title =$val['title'];
					$discount =$val['discount'];
					continue;
				} 
				
				break;
			}
		}

		$result = array(
				'title'=>$title,
				'pay_amount'=>$pay_amount,
				'discount'=>$discount?$discount*100:1,
			);
		return $result;
	}
}
