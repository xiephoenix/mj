<?php
class elephant_touchcard_protocol extends Protox
{
    protected $fields = array(
        'event'=>'string',
        'throw_seatid'=>'int',
        'left_pai'=>'int',
        'present_flower_nums'=>'int',
        'pai_nums'=>'int',
        'pai'=>'array',
        'present_flower'=>'array',
        'hu'=>'int',
        'gang'=>'int',
        'combox_gang_pai'=>'array',
    );
}
class elephant_touchcard_data_protocol extends Protox
{
    protected $fields = array(
					'throw_seatid'=>'int',
					'left_pai'=>'int',
					'pai'=>'array',
					'present_flower'=>'array',
    );
}