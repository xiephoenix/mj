<?php
class index_login_protocol extends Protox
{
    protected $fields = array(
        'uid' => 'int',
        'fromuser'=>'string',
        'username' => 'string',
        'headimgurl'=>'string',
        'nickname' => 'string',
        'gamepoint' => 'int',
        'gold' => 'int',
		'roomid' => 'int',
		'seatid' => 'int',
    );
}