<?php
class elephant_offline_protocol extends Protox
{
    protected $fields = array(
        'seatid' => 'int',
        'seats'=>'string',
        'leave_info'=>'array.leave_data',
    );
}

class leave_data_protocol extends Protox
{
    protected $fields = array(
            'uid'=>'int',
            'nickname'=>'string',
            'headimgurl' => 'string',
            'seatid'=>'int',
    );
}