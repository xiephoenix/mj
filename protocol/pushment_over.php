<?php
class pushment_over_protocol extends Protox
{   
    protected $fields = array(
        'pai_list'=>'array.pai_data',
        'scorelist'=>'array.member_data',
        'giveup'=>'int',
    );
}
class pai_data_protocol extends Protox{
    protected $fields = array(
            'front_pai'=>'array',
            'handpai'=>'array',
            'pai_type' => 'array.score_data',
            'all_taishu'=>'int',
            'pai'=>'int',
    );
}
class score_data_protocol extends Protox
{
    protected $fields = array(
        			'title'=>'string',
					'score'=>'int',
    );
}
class member_data_protocol extends Protox
{
    protected $fields = array(
        			'nickname'=>'string',
					'score'=>'int',
                    'seatid'=>'int',
                    'point'=>'int',
    );
}