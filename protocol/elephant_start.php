<?php
class elephant_start_protocol extends Protox
{
    protected $fields = array(
    	'room_info'=>'array.room_data',
        'seats'=>'string',
        'start'=>'int',
        'die'=>'array',
        'lefttime'=>'int',
    );
}
class room_data_protocol extends Protox
{
    protected $fields = array(
            'owner'=>'int',
            'banker'=>'int',
            'double'=>'int',
            'roomid' => 'int',
            'time_way'=>'int',
            'time_key'=>'int',
            'create_time'=>'int',
            'system_time' => 'int',
            'present_quan'=>'int',
    );
}