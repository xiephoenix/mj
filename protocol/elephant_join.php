<?php
class elephant_join_protocol extends Protox
{
    protected $fields = array(
        'owner_info'=>'array.owner_data',
        'room_info'=>'array.room_data',
        'seat_info'=>'array.seat_data',
    );
}
class owner_data_protocol extends Protox
{
    protected $fields = array(
            'uid'=>'int',
            'seatid'=>'int',
            'nickname'=>'string',
            'headimgurl'=>'string',
    );
}
class room_data_protocol extends Protox
{
    protected $fields = array(
            'owner'=>'int',
            'double'=>'int',
            'roomid' => 'int',
            'time_way'=>'int',
            'time_key'=>'int',
            'create_time'=>'int',
            'system_time' => 'int',
            'present_quan'=>'int',
    );
}
class seat_data_protocol extends Protox
{
    protected $fields = array(
                    'uid'=>'int',
                    'seatid'=>'int',
                    'nickname'=>'string',
                    'headimgurl'=>'string',
    );
}