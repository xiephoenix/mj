<?php
class elephant_info_protocol extends Protox
{
    protected $fields = array(
        'id' => 'int',
		'seatid' => 'int',
		'step' => 'int',
		'current' => 'int',
		'users' => 'array.elephant_info_user',
    );
}