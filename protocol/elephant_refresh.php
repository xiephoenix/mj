<?php
class elephant_refresh_protocol extends Protox
{
    protected $fields = array(
        'uid'=>'int',
        'seatid' => 'int',
        'state'=>'int',
        'owner_info'=>'array.owner_data',
        'room_info'=>'array.room_data',
        'seat_info'=>'array.seat_data',
        'pai_info'=>'array.pai_data',
    );
}

class owner_data_protocol extends Protox
{
    protected $fields = array(
            'uid'=>'int',
            'seatid'=>'int',
            'nickname'=>'string',
            'headimgurl'=>'string',
    );
}
class room_data_protocol extends Protox
{
    protected $fields = array(
            'owner'=>'int',
            'banker'=>'int',
            'double'=>'int',
            'roomid' => 'int',
            'time_way'=>'int',
            'time_key'=>'int',
            'create_time'=>'int',
            'system_time' => 'int',
            'present_quan'=>'int',
    );
}
class seat_data_protocol extends Protox
{
    protected $fields = array(
                    'uid'=>'int',
                    'seatid'=>'int',
                    'nickname'=>'string',
                    'headimgurl'=>'string',
    );
}
class pai_data_protocol extends Protox
{
    protected $fields = array(
    				'uid'=>'int',
    				'seatid'=>'int',
                    'handpai'=>'array',
                    'handpai_count'=>'int',
                    'front'=>'array',
                    'flower'=>'array',
                    'throw_front'=>'array',
    );
}