<?php
class elephant_reconnet_protocol extends Protox
{
    protected $fields = array(
        'owner_info'=>'array.owner_data',
        'room_info'=>'array.room_data',
        'seat_info'=>'array.seat_data',
        'game_status'=>'int',
        'game_info'=>'array.game_data',
    );
}
class owner_data_protocol extends Protox
{
    protected $fields = array(
            'uid'=>'int',
            'seatid'=>'int',
            'nickname'=>'string',
            'headimgurl'=>'string',
    );
}
class game_data_protocol extends Protox
{
    protected $fields = array(
            'seatid'=>'int',
            'throw_seatid'=>'int',
            'type'=>'int',
            'pai'=>'int',
            'eat'=>'int',
            'peng'=>'int',
            'gang'=>'int',
            'hu'=>'int',
            'combox_eat_pai'=>'array',
            'combox_peng_pai'=>'array',
            'combox_gang_pai'=>'array',

    );
}
class room_data_protocol extends Protox
{
    protected $fields = array(
            'owner'=>'int',
            'banker'=>'int',
            'double'=>'int',
            'roomid' => 'int',
            'time_way'=>'int',
            'time_key'=>'int',
            'lefttime'=>'int',
            'system_time' => 'int',
            'present_quan'=>'int',
            'left_pai'=>'int',
    );
}
class seat_data_protocol extends Protox
{
    protected $fields = array(
                    'uid'=>'int',
                    'seatid'=>'int',
                    'point'=>'int',
                    'voice_url'=>'string',
                    'nickname'=>'string',
                    'headimgurl'=>'string',
                    'frontpai'=>'string',
                    'handpai'=>'array',
                    'present_flower_nums'=>'int',
                    'present_flower'=>'array',
                    'other_handpai'=>'int',
                    'other_frontpai'=>'int',
                    'other_flowerpai'=>'int',
                    'throw_front'=>'array',
    );
}