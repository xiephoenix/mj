<?php
class elephant_info_user_protocol extends Protox{
	protected $fields = array(
        'uid' => 'int',
        'seatid' => 'int',
		'state' => 'int',
		'total' => 'int',
		'point' => 'int',
		'name' => 'string',
		'avatar' => 'string',
		'offline' => 'int',
    );
}