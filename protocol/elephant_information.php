<?php
class elephant_information_protocol extends Protox
{
    protected $fields = array(
        'headimgurl'=>'string',
        'nickname'=>'string',
        'victory'=>'int',
        'escape'=>'int',
        'match'=>'int',
    );
}