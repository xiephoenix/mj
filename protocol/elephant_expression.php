<?php
class elephant_expression_protocol extends Protox
{
    protected $fields = array(
       	'type'=>'string',
       	'emoji'=>'string',
       	'seatid'=>'int',
       	'receive_seatid'=>'int',
    );
}