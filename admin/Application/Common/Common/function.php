<?php
function getLevel($amount){
    	$level_permit =array(
				10=>array(
					'title'=>'VIP1',
					'discount'=>0.98,
				),
				50=>array(
					'title'=>'VIP2',
					'discount'=>0.97,
				),
				100=>array(
					'title'=>'VIP3',
					'discount'=>0.96,
				),
				200=>array(
					'title'=>'VIP4',
					'discount'=>0.95,
				),
				400=>array(
					'title'=>'VIP5',
					'discount'=>0.94,
				),
				700=>array(
					'title'=>'VIP6',
					'discount'=>0.93,
				),
				1100=>array(
					'title'=>'VIP7',
					'discount'=>0.92,
				),
				1600=>array(
					'title'=>'VIP8',
					'discount'=>0.9,
				),
				2200=>array(
					'title'=>'VIP9',
					'discount'=>0.88,
				),
			);
    	if(isset($level_permit[$amount])) return $level_permit[$amount]['title'];
    	return "VIP0";
    }