<?php
/**
 * @Author: anchen
 * @Date:   2016-11-13 14:25:02
 * @Last Modified by:   anchen
 * @Last Modified time: 2016-11-14 20:45:02
 */
namespace Common\Controller;



use Think\Controller;
use Think\Auth;

class AdminController extends Controller{

     public function _initialize() {
        //session不存在时，不允许直接访问
          $not_check = array( 'Login/index', 'Login/dologin', 'Login/logout','Index/top_menu','Index/left_menu','Index/main','Index/main','Index/foot','/adm/h-ui/lib/webuploader/0.1.5/server/fileupload.php');
        if (in_array(CONTROLLER_NAME . '/' . ACTION_NAME, $not_check)) {
                return true;
            }
       if (session('admin_status')!=2&& !session('customer_id')) {
           $this->error('还没有登录，正在跳转到登录页', U('Login/index'));
       }
        $auth = new Auth();
        $customer_id=session('customer_id');
        if (!$auth->check(CONTROLLER_NAME . '/' . ACTION_NAME, $customer_id)) {
            $this->error('暂无权限');
        }
        $this->assign($_REQUEST);
    }

}