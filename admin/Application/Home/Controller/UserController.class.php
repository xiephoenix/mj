<?php
namespace Home\Controller;

use Common\Controller\AdminController;
use Think\Controller;

class UserController extends AdminController
{
    public function index( $p = 1)
    {	
    	
    	$users = M('users');
        $member =M('member');
    	$where=array();
    	//$title != '' && $where['title'] = array('like', "%$title%");
    	$list = $users->where($where)->order('id desc')->page($p,10)->select();
        $count = $users->where($where)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, 10); // 实例化分页类 传入总记录数和每页显示的记录数
        $show = $Page->show(); // 分页显示输出

        $this->assign('page', $show); // 赋值分页输出
        $this->assign('count', $count);
        $temp_list =array();
        if(is_array($list)&&$list){
            foreach ($list as $key => $value) {
                $memberInfo = $member->where(array('fromuser'=>$value['fromuser']))->find();
                $value['nickname']=isset($memberInfo['nickname'])?$memberInfo['nickname']:'未知';
                $temp_list[]=$value;
            }
        }

        $this->assign('list', $temp_list); // 赋值数据集
    	$this->display();
    }
   
}