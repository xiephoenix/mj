<?php
namespace Home\Controller;

use Common\Controller\AdminController;
use Think\Controller;

class GoldController extends AdminController
{
    public function index($beg_time = '',$end_time='', $p = 1)
    {	
    	if($beg_time) $beg_stamp=strtotime($beg_time);
    	if($end_time) $end_stamp=strtotime($end_time);
    	$gold_types =M('gold_types');
    	$where=array();
    	//$title != '' && $where['title'] = array('like', "%$title%");
    	if($beg_time) $where['created']=array('egt',$beg_stamp);
    	if($end_time) $where['created']=array('elt',$end_stamp);
    	$list = $gold_types->where($where)->order('id desc')->page($p,10)->select();
        $count = $gold_types->where($where)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, 10); // 实例化分页类 传入总记录数和每页显示的记录数
        $show = $Page->show(); // 分页显示输出

        $this->assign('page', $show); // 赋值分页输出
        $this->assign('count', $count);
        $this->assign('list', $list); // 赋值数据集
    	$this->display();
    }
   public function add($id=""){
        $title ='添加';
        $singleRow='';
        if($id){
            $goldTable =M('gold_types');
            $singleRow =$goldTable->where(array('id'=>$id))->find();
            $title ='修改';
        }
        $this->assign('title',$title);
        $this->assign('singleRow',$singleRow);
        $this->assign('id',$id);
        $this->display();
   }
   public function save(){
        $post =I('post.');
        $id =$post['id'];
        $data =$post['data'];
        $goldTable =M('gold_types');
        $map['gold']=$data['gold'];
        $count =$goldTable->where($map)->count();
        if($count) {
            $data =array('code'=>1,'message'=>'钻石数值已存在.');
            $this->ajaxReturn($data);
            //$this->error('数值已经存在了.', U('Gold/add',array('id'=>$id)));
        }
        if($id){
            $goldTable->where(array('id'=>$id))->save($data);
            $data =array('code'=>0,'message'=>'修改成功.','url'=>U('Gold/index'));
            $this->ajaxReturn($data);
        }else{
            $data['created']=time();
            $goldTable->add($data);
            $data =array('code'=>0,'message'=>'添加成功.','url'=>U('Gold/index'));
            $this->ajaxReturn($data);
        }

   }
   public function del(){
        $id =I('post.id');
        if($id){
            $goldTable =M('gold_types');
            $goldTable->where(array('id'=>$id))->delete();
            $data =array('code'=>0,'message'=>'删除成功.');
            $this->ajaxReturn($data);
        }
   }
}