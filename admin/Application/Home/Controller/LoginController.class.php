<?php
namespace Home\Controller;

use Think\Controller;

class LoginController extends Controller{

	public function index(){
		$this->display();
	}

	public function dologin(){
		$post = I('post.');
        $map['email']=$post['username'];
        $customer=D('customer');
        //验证码检测
        $vertify =$post['input_code'];
        $verify = new \Think\Verify();
        if(!$verify->check($vertify)){
            $data =array('code'=>1,'message'=>'验证码错误');
            $this->ajaxReturn($data);
        }

        $customerInfo = $customer->where($map)->find();
        if(isset($customerInfo['user_pass'])){
            if($customerInfo['user_pass']==md5($post['pass'])){
                session('admin_status',2);
                session('customer_id',$customerInfo['id']);
                session('username',$customerInfo['contact_user']);
                $data =array('code'=>0,'message'=>'登录成功','url'=>U('Index/index'));
                $this->ajaxReturn($data);
            }else{
                $data =array('code'=>1,'message'=>'密码不正确');
                $this->ajaxReturn($data);
            }
        }else{
            $data =array('code'=>1,'message'=>'不存在该账户');
            $this->ajaxReturn($data);
        }
	}
    public function  logout(){
        session('admin_status',null);
        session('customer_id',null);
        session('username',null);
        //$this->redirect(U('login/index'));
    }
}