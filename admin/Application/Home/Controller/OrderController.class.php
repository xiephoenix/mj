<?php
namespace Home\Controller;

use Common\Controller\AdminController;
use Think\Controller;

class OrderController extends AdminController
{
    public function index($beg_time = '',$end_time='', $p = 1)
    {	
    	if($beg_time) $beg_stamp=strtotime($beg_time);
    	if($end_time) $end_stamp=strtotime($end_time);
    	$order =M('order');
    	$where=array();
    	//$title != '' && $where['title'] = array('like', "%$title%");
    	if($beg_time) $where['created']=array('egt',$beg_stamp);
    	if($end_time) $where['created']=array('elt',$end_stamp);
    	$list = $order->where($where)->order('id desc')->page($p,10)->select();
        $count = $order->where($where)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, 10); // 实例化分页类 传入总记录数和每页显示的记录数
        $show = $Page->show(); // 分页显示输出

        $this->assign('page', $show); // 赋值分页输出
        $this->assign('count', $count);
        $this->assign('list', $list); // 赋值数据集
    	$this->display();
    }
   
}