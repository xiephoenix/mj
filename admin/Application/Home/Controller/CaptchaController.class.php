<?php
namespace Home\Controller;


use Think\Controller;

class CaptchaController extends Controller
{
    public function index()
    {	
    	$Verify =     new \Think\Verify();
		$Verify->fontSize = 18;
		$Verify->length   = 4;
		$Verify->useNoise = true;
		$Verify->imageW=130;
		$Verify->imageH=41;
		$Verify->entry();
    }
    
}