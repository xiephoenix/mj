<?php
namespace Home\Controller;

use Common\Controller\AdminController;
use Think\Controller;

class BannerController extends AdminController
{
    public function index($beg_time = '',$end_time='', $p = 1)
    {	
    	if($beg_time) $beg_stamp=strtotime($beg_time);
    	if($end_time) $end_stamp=strtotime($end_time);
    	$banners =M('banners');
    	$where=array();
    	$list = $banners->where($where)->order('id desc')->page($p,10)->select();
        $count = $banners->where($where)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, 10); // 实例化分页类 传入总记录数和每页显示的记录数
        $show = $Page->show(); // 分页显示输出

        $this->assign('page', $show); // 赋值分页输出
        $this->assign('count', $count);
        $this->assign('list', $list); // 赋值数据集
    	$this->display();
    }
    public function add($id=""){
        $title ='添加';
        $singleRow='';
        if($id){
            $goldTable =M('banners');
            $singleRow =$goldTable->where(array('id'=>$id))->find();
            $title ='修改';
        }
        $this->assign('title',$title);
        $this->assign('singleRow',$singleRow);
        $this->assign('id',$id);
        $this->display();
   }
   public function bsave(){
        $bannerTable =M('banners');
        $post =I('post.');
        $data['title']=$post['title'];
        $id=I('post.id');
        if($post['pic']) $data['pic']=$post['pic'];
         if($id){
            $bannerTable->where(array('id'=>$id))->save($data);
            $data =array('code'=>0,'message'=>'修改成功.','url'=>U('Banner/index'));
            $this->ajaxReturn($data);
        }else{
            $data['created']=time();
            $bannerTable->add($data);
            $data =array('code'=>0,'message'=>'添加成功.','url'=>U('Banner/index'));
            $this->ajaxReturn($data);
        }
   }
}