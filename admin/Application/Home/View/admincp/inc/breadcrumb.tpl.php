<nav class="breadcrumb">
    <i class="iconfont">&#xf012b;</i>
    <?=!empty($crumb)?implode(' <span class="c-gray en">&gt;</span> ', $crumb):'首页';?>
    <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" >
        <i class="icon-refresh"></i>
    </a>
</nav>