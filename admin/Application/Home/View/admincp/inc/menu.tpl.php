<aside class="Hui-aside">
    <input runat="server" id="divScrollValue" type="hidden" value="" />
    <div class="menu_dropdown bk_2">
        <?php
        $pub_admin_menu = DM('fwk:adminmenu.all', 'i', array('am_isdel' => 'N', 'am_status' => 'Y'), 'ORDER BY am_px ASC');
        if (!empty($pub_admin_menu)) {
            $pub_admin_info = CPUSER::v('admin');
            if ($pub_admin_info['admin_super'] == 'Y') {
                $pub_mine_menu = $pub_admin_menu;
            } else {
                $arrstr = DM('fwk:admingrouphasmenu.mymenu', 'i', $pub_admin_info['admin_gid']);
                $pub_mine_menu = array();
                $arr = @explode(',', $arrstr);
                foreach ($pub_admin_menu as $k => $v) {
                    if (in_array($v['am_id'], $arr) == true) {
                        $pub_mine_menu[$k] = $v;
                    }
                }
                unset($arr);
                unset($arrstr);
            }
            if (!empty($pub_mine_menu)) {
                foreach ($pub_mine_menu as $k => $v) {
                    if ($v['am_fid'] == 0) {
                        ?>
                        <dl id="menu-id-<?= $v['am_id']; ?>">
                            <dt><i class="<?= $v['am_icon']; ?>"></i> <?= $v['am_name']; ?><i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>
                            <dd>
                                <ul>
                                    <?php
                                    foreach ($pub_admin_menu as $kk => $vv) {
                                        if ($vv['am_fid'] == $v['am_id']) {
                                            ?>
                                            <li><a _href="<?= $vv['am_url']; ?>" href="javascript:void(0);"><?= $vv['am_name']; ?></a></li>
                                            <?php
                                        }
                                    }
                                    ?> 
                                </ul>
                            </dd>
                        </dl>
                        <?php
                        if ($v['am_name'] == '内容管理') {
                            $category = DM('fwk:category.all', 'i');
                            foreach ($category as $m => $n) {
                                if ($n['ca_fid'] == 0) {
                                    ?>
                                    <dl id="menu-id-info-<?=$n['ca_id'];?>">
                                        <dt><i class="icon-file-alt"></i> <?=$n['ca_name'];?><i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>
                                        <dd>
                                            <ul>
                                                <?php
                                                foreach ($category as $mm => $nn) {
                                                    if ($nn['ca_fid'] == $n['ca_id']) {
                                                        ?>
                                                        <li><a _href="<?= RURL . 'admincp.article/tab-lists/cafid-' . $nn['ca_fid'] . '/caid-' . $nn['ca_id'] . '/'; ?>" href="javascript:void(0);"><?= $nn['ca_name']; ?></a></li>
                                                        <?php
                                                    }
                                                }
                                                ?> 
                                            </ul>
                                        </dd>
                                    </dl>
                                    <?php
                                }
                            }
                        }
                    }
                }
            }
        }
        ?>
        <?php
        if (USER::v('admin_super') == 'Y') {
            ?>
            <dl id="menu-admin">
                <dt><i class="icon-key"></i> 系统权限<i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>
                <dd>
                    <ul>
                        <li><a _href="<?= RURL . 'admincp.siteadmin/tab-editpwd'; ?>" href="javascript:void(0)">修改密码</a></li>
                        <li><a _href="<?= RURL . 'admincp.siteadmin/tab-menu'; ?>" href="javascript:void(0)">管理菜单</a></li>
                        <li><a _href="<?= RURL . 'admincp.siteadmin/tab-group'; ?>" href="javascript:void(0)">管理分组</a></li>
                        <li><a _href="<?= RURL . 'admincp.siteadmin/tab-role'; ?>" href="javascript:void(0)">管理角色</a></li>
                        <li><a _href="<?= RURL . 'admincp.siteadmin/tab-lists'; ?>" href="javascript:void(0)">管理人员</a></li>
                    </ul>
                </dd>
            </dl>
            <dl id="menu-system">
                <dt><i class="icon-cogs"></i> 系统管理<i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>
                <dd>
                    <ul>
                        <li><a _href="<?= RURL . 'admincp.siteconfig/tab-cfg'; ?>" href="javascript:void(0)">基本设置</a></li>
                        <li><a _href="<?= RURL . 'admincp.siteconfig/tab-links'; ?>" href="javascript:void(0)">友情链接</a></li>
						<li><a _href="/Admin/wechat/index.html" href="javascript:void(0)">微信关键词回复</a></li>
                    </ul>
                </dd>
            </dl>
        <?php } ?>
    </div>
</aside>