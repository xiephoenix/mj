<div  class="u-btn u-btn-upload"><div id="<?= $uploadButtonId; ?>"></div></div>
<div>
    <span id="upload_queeid"></span>
    <div id="<?= $uploadSaveValue; ?>_value" style="display:none;"></div>
</div>
<?php $timestamp = time(); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#<?= $uploadButtonId; ?>').uploadify({
            'uploader': '<?= RURL . 'admincp.uploadimage'; ?>',
            'buttonClass': '',
            'buttonText': '选择文件',
            'swf': '/s/swf/uploadify.swf',
            'fileSizeLimit': '20MB',
            'removeCompleted': false,
            'multi': false,
            'width': '80',
            removeTimeout: 1,
            'prevent_swf_caching': true,
            'fileTypeExts': '<?= SITE_UPLOAD_IMG_FILE_EXTS; ?>',
            'queueID': 'upload_queeid',
            'formData': {
                'SESSION_ID': '<?= session_id(); ?>',
                'timestamp': '<?= $timestamp; ?>',
                'token': '<?= md5(SITE_UPLOAD_SECRET . $timestamp); ?>'
            },
            'overrideEvents': ['onSelectError', 'onDialogClose', 'onUploadError', 'onComplete', 'onError'],
            'onSelectError': function (file, errorCode, errorMsg) {
                switch (errorCode) {
                    case SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:
                        alert('选择的文件过多，你一次最多选择10个附件进行上传。');
                        break;
                    case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
                        alert('文件 [' + file.name + '] 的文件大小超出范围(20MB)，请将该文件分割成几个20MB以内的压缩包上传。');
                        break;
                    case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
                        alert('文件 [' + file.name + '] 的文件大小没有内容，请确认文件是否正确。');
                        break;
                    case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
                        alert('文件 [' + file.name + '] 的格式不支持上传类型，请将该文件打成压缩包上传。');
                        break;
                }

            },
            'onDialogClose': function (queueData) {
            },
            'onUploadError': function (file, errorCode, errorMsg, errorString) {
                alert('上传文件 [' + file.name + '] 发生错误，原因可能是 ' + errorString);
            },
            'onUploadSuccess': function (file, data, response) {
                if (response == true) {
                    var json_data = $.parseJSON(data);
                    if (json_data.code == 0) {
                        var json = json_data.ext;
                        $('#<?= $uploadSaveValue; ?>').val(json.file_name);
                        $('#<?= $uploadSaveValue; ?>_value').append('<input  type="hidden" id="hasup_id_' + file.id + '" name="hasup_file" ext="' + json.ext + '"  uid="' + json.uid + '"  oldname="' + json.old_name + '" value="' + json.file_name + '" />');
                        $('#upload_queeid').empty().hide();
                    }
                }

            }
        });

    });

</script>