<header class="Hui-header cl">
    <a class="Hui-logo l" title="<?=L('admincp_title');?> <?=L('SiteVersionId');?>" href="/"><?=L('admincp_title');?></a>
    <a class="Hui-logo-m l" href="/" title="<?=L('admincp_title');?> <?=L('SiteVersionId');?>"><?=L('admincp_title');?></a>
    <span class="Hui-subtitle l"><?=L('SiteVersionId');?></span>
    <span class="Hui-userbox">
        <span class="c-white"><?=(USER::v('admin_super') == 'Y')?'超级管理员':'当前登录';?>：<?=CPUSER::v('admin_nick');?></span>
        <a class="btn btn-danger radius ml-10" href="<?=RURL;?>admincp.loginout/" title="退出"><i class="icon-off"></i> 退出</a>
    </span>
    <a aria-hidden="false" class="Hui-nav-toggle" href="#"></a>
</header>
