<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!--[if lt IE 9]>
<script type="text/javascript" src="/s/adm/lib/html5.js"></script>
<script type="text/javascript" src="/s/adm/lib/respond.min.js"></script>
<script type="text/javascript" src="/s/adm/lib/PIE_IE678.js"></script>
<![endif]-->
<link href="/s/adm/css/H-ui.min.css" rel="stylesheet" type="text/css" />
<link href="/s/adm/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
<link href="/s/adm/css/style.css" rel="stylesheet" type="text/css" />
<link href="/s/adm/lib/font-awesome/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!--[if IE 7]>
<link href="/s/adm/lib/font-awesome/font-awesome-ie7.min.css" rel="stylesheet" type="text/css" />
<![endif]-->
<link href="/s/adm/lib/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
<!--[if IE 6]>
<script type="text/javascript" src="/s/adm/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<title>平台管理 | 站点名称：<?= L('SiteTitle'); ?> | 环境配置：<?= ENVIRONMENT; ?></title>
<script type="text/javascript" src="/s/adm/lib/jquery.min.js"></script>

