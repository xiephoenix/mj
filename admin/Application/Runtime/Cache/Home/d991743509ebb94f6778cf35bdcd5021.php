<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <?php TPL::plugin('admincp/inc/head', array('data' => $data)); ?>
    </head>
    <body>
        <?php TPL::plugin('admincp/inc/header', array('data' => $data)); ?>
        <?php TPL::plugin('admincp/common/menu', array('data' => $data)); ?>
        <div class="dislpayArrow"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a></div>
        <section class="Hui-article-box">
            <div id="Hui-tabNav" class="Hui-tabNav">
                <div class="Hui-tabNav-wp">
                    <ul id="min_title_list" class="acrossTab cl">
                        <li class="active"><span title="工作台" data-href="about:blank">工作台</span><em></em></li>
                    </ul>
                </div>
                <div class="Hui-tabNav-more btn-group"><a id="js-tabNav-prev" class="btn radius btn-default size-S" href="javascript:;"><i class="icon-step-backward"></i></a><a id="js-tabNav-next" class="btn radius btn-default size-S" href="javascript:;"><i class="icon-step-forward"></i></a></div>
            </div>
            <div id="iframe_box" class="Hui-article">
                <div class="show_iframe">
                    <div style="display:none" class="loading"></div>
                    <?php if (CPUSER::uid() == '123456789') { ?>
                        <iframe id="iframe" name="iframe" scrolling="yes" frameborder="0" src="about:blank"></iframe>
                    <?php } else { ?>
                        <?php $pub_admin_info = CPUSER::v('admin');?>
                        <?php if($pub_admin_info['admin_super']=='Y'):?>
                        <iframe id="iframe" name="iframe" scrolling="yes" frameborder="0" src="/p/admincp.order/act-lists"></iframe>
                        <?php endif;?>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php TPL::plugin('admincp/inc/footer', array('data' => $data)); ?>
    </body>
</html>