<?php if (!defined('THINK_PATH')) exit();?><html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="/admin/Public/Admin/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/admin/Public/Admin/css/default.css">
        <script type="text/javascript" src="/admin/Public/Admin/js/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="/admin/Public/Admin/js/common.js"></script>
        <link rel="stylesheet" href="/admin/Public/Admin/css/main2.css"/>
    </head>
</html>
<div class="main-header" style="min-width:1200px;">
   <h1>
      <img src="/admin/Public/Admin/img/yl_head.png" />
   </h1>
    <div class="title">
        <div class="pull-right">
		  <span>欢迎您 <?php echo (session('username')); ?></span>
            <?php if($_SESSION['headquarters_id']!= null): ?><img src="/admin/Public/Admin/img/yl_logout.png" onclick="back()"/>
                <?php else: ?>
                <img src="/admin/Public/Admin/img/yl_logout.png" onclick="log_out()"/><?php endif; ?>
		</div>
    </div>
</div>
<script>
    function log_out() {
        $.post("<?php echo U('Login/logout');?>", function () {
            parent.window.location.reload();
        });
    }

    function back(){
        $.post("<?php echo U('Index/back');?>", function (data) {
            parent.window.location.reload();
        });
    }
</script>