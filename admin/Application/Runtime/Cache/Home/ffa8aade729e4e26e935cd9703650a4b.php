<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!--[if lt IE 9]>
<script type="text/javascript" src="/admin/Public/adm/h-ui/lib/html5shiv.js"></script>
<script type="text/javascript" src="/admin/Public/adm/h-ui/lib/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="/admin/Public/adm/h-ui/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="/admin/Public/adm/h-ui/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/admin/Public/adm/h-ui/lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="/admin/Public/adm/h-ui/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="/admin/Public/adm/h-ui/static/h-ui.admin/css/style.css" />
<!--[if IE 6]>
<script type="text/javascript" src="/admin/Public/adm/h-ui/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<title> 钻石充值管理</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 钻石充值管理 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
  <form action="" id='form_search'>
  <div class="text-c"> 日期范围：
    <input type="text" name="beg_time" onfocus="WdatePicker({ maxDate:'#F<?php echo ($dp["$D('datemax') or '%y-%M-%d'"]); ?>' })" id="datemin" class="input-text Wdate" style="width:120px;">
    -
    <input type="text" name="end_time" onfocus="WdatePicker({ minDate:'#F<?php echo ($dp["$D('datemin')"]); ?>',maxDate:'%y-%M-%d' })" id="datemax" class="input-text Wdate" style="width:120px;">
    <!-- <input type="text" class="input-text" style="width:250px" placeholder="输入" id="" name=""> -->
    <button type="submit" class="btn btn-success radius" id="" name=""><i class="Hui-iconfont">&#xe665;</i> 搜记录</button>
  </div>
  </form>
  <div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="r">共有数据：<strong><?php echo ($count); ?></strong> 条</span> </div>
  <div class="mt-20">
  <table class="table table-border table-bordered table-hover table-bg table-sort">
    <thead>
      <tr class="text-c">
        <th width="25"><input type="checkbox" name="" value=""></th>
        <th width="80">ID</th>
        <th width="100">人民币</th>
        <th width="40">钻石数</th>
        <th width="90">添加时间</th>
        <th width="150">vip等级（默认）</th>
        <th width="100">操作</th>
      </tr>
    </thead>
    <tbody>
    <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr class="text-c">
        <td><input type="checkbox" value="<?php echo ($vo["id"]); ?>" name="id"></td>
        <td><?php echo ($vo["id"]); ?></td>
        <td><?=$vo['amount']/100?></td>
        <td><?=$vo['price']/100?></td>
        <td><?=$vo['created']?date('Y-m-d H:i:s',$vo['created']):''?></td>
        <td><?php echo getLevel($vo['amount']/100);?></td>
        <td class="td-manage"><a title="编辑" href="javascript:;" onclick="member_edit('编辑','javascript:;','4','','510')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i></a> <a title="删除" href="javascript:;" onclick="member_del(this,'1')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>
      </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
  </table>
  <div class="page-list"><?php echo ($page); ?></div>
  </div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/admin/Public/adm/h-ui/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="/admin/Public/adm/h-ui/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/admin/Public/adm/h-ui/static/h-ui/js/H-ui.min.js"></script> 
<script type="text/javascript" src="/admin/Public/adm/h-ui/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/admin/Public/adm/h-ui/lib/My97DatePicker/4.8/WdatePicker.js"></script> 
<script type="text/javascript" src="/admin/Public/adm/h-ui/lib/datatables/1.10.0/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="/admin/Public/adm/h-ui/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
$(function(){
  $('.table-sort').dataTable({
    "bPaginate":false,
    "bFilter":false,
    "aaSorting": [[ 1, "desc" ]],//默认第几个排序
    "bStateSave": true,//状态保存
    "aoColumnDefs": [
      //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
      {"orderable":false,"aTargets":[0,5,6]}// 制定列不参与排序
    ]
  });
  
});
/*用户-添加*/
function member_add(title,url,w,h){
  layer_show(title,url,w,h);
}
/*用户-查看*/
function member_show(title,url,id,w,h){
  layer_show(title,url,w,h);
}



/*用户-编辑*/
function member_edit(title,url,id,w,h){
  layer_show(title,url,w,h);
}
/*密码-修改*/
function change_password(title,url,id,w,h){
  layer_show(title,url,w,h);  
}
/*用户-删除*/
function member_del(obj,id){
  layer.confirm('确认要删除吗？',function(index){
    $.ajax({
      type: 'POST',
      url: '',
      dataType: 'json',
      success: function(data){
        $(obj).parents("tr").remove();
        layer.msg('已删除!',{icon:1,time:1000});
      },
      error:function(data) {
        console.log(data.msg);
      },
    });   
  });
}
</script> 
</body>
</html>