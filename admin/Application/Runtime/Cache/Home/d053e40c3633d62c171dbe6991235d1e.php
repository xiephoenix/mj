<?php if (!defined('THINK_PATH')) exit();?><html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="/admin/Public/Admin/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/admin/Public/Admin/css/default.css">
        <link rel="stylesheet" type="text/css" href="/admin/Public/Admin/css/main2.css">
        <script type="text/javascript" src="/admin/Public/Admin/js/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="/admin/Public/Admin/js/menu.js"></script>
    </head>
</html>
<div class="main-menu w_100" style="overflow-y: auto">

        <ul class="menu">
            <li>
                <a href="javascript:;"><i class="icons icons-list"></i>礼品管理</a>
                <ul style="display:none;">
                        <li ><a href="<?php echo U('Gift/index');?>" target="rightFrame">礼品列表</a></li>
                        <!-- <li ><a href="<?php echo U('Store/index');?>" target="rightFrame">商家列表</a></li>


                        <!-- <li ><a href="<?php echo U('Product/index');?>" target="rightFrame">商品列表</a></li>


                        <li ><a href="<?php echo U('Category/index');?>" target="rightFrame">分类管理</a></li> -->

                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="icons icons-member"></i>统计数据</a>
                <ul style="display:none;">

                        <li class="omega"><a href="<?php echo U('Gift/exchange');?>" target="rightFrame">兑换记录</a></li>
                        <li class="omega"><a href="<?php echo U('Statis/index');?>" target="rightFrame">访问数据量</a></li>
                        <li class="omega"><a href="<?php echo U('Statis/vister');?>" target="rightFrame">访问量图表</a></li>
                        <li class="omega"><a href="<?php echo U('Statis/register');?>" target="rightFrame">注册量图表</a></li>
                </ul>
            </li>
            <!-- <li>
                <a href="javascript:;"><i class="icons icons-cog"></i>权限管理</a>
                <ul style="display:none;">

                        <li class="omega"><a href="<?php echo U('AuthGroup/index');?>" target="rightFrame">角色管理</a></li>

                </ul>
            </li> -->


        </ul>


</div>

<script>
    left_menu();//左侧菜单js调用
    //退出
    // function log_out() {
    //     $.post("<?php echo U('Login/logout');?>", function () {
    //         parent.window.location.reload();
    //     });
    // }
</script>