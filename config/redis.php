<?php
return array(
	'user' => array(
		'host' => '127.0.0.1',
		'port' => 6379,
		'prefix' => 'user::',
		'handle' => 'redisUser',
		'pconnect' => 1,
	),
	'game' => array(
		'host' => '127.0.0.1',
		'port' => 6379,
		'prefix' => 'game::',
		'handle' => 'redisGame',
		'pconnect' => 1,
	),
	'wechat' => array(
		'host' => '127.0.0.1',
		'port' => 6379,
		'prefix' => 'wechat::',
		'handle' => 'redisWechat',
		'pconnect' => 1,
	),
	'match'=> array(
		'host' => '127.0.0.1',
		'port' => 6379,
		'prefix' => 'match::',
		'handle' => 'redisMatch',
		'pconnect' => 1,
	),
	'accesstoken'=>array(
		'host' => '127.0.0.1',
		'port' => 6379,
		'prefix' => 'accesstoken::',
		'handle' => 'redisAccesstoken',
		'pconnect' => 1,
	),
	'message'=>array(
		'host' => '127.0.0.1',
		'port' => 6379,
		'prefix' => 'message::',
		'handle' => 'redisMessage',
		'pconnect' => 1,
	),
	'log'=>array(
		'host' => '127.0.0.1',
		'port' => 6379,
		'prefix' => 'log::',
		'handle' => 'redisLog',
		'pconnect' => 1,
	),
);