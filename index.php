<?php
define('APP_PATH', dirname(__FILE__));
define ("MYPATH", dirname(__FILE__));
require APP_PATH.'/vendor/autoload.php';
$app = new Yaf_Application(APP_PATH . '/config/application.ini');
$app->bootstrap()->run();