<?php
class Bootstrap extends Yaf_Bootstrap_Abstract
{	
	private $config = null;
		
	public function _initSession($dispatcher)
    {
        Yaf_Session::getInstance()->start();
        define('REQUEST_METHOD', strtoupper($dispatcher->getRequest()->getMethod()));
    }
	
	function _init()
	{
		Yaf_Loader::import('BaseController.php');
		Yaf_Loader::import('WechatController.php');
        Yaf_Loader::import('BaseModel.php');
		Yaf_Loader::import('log.php');
		$this->config = Yaf_Application::app()->getConfig();
		Yaf_Registry::set("config", $this->config);

		Yaf_Dispatcher::getInstance()->disableView();
		
		Protox::init(array(
			'path' => APP_PATH . '/protocol/',
		));
	}
	public function _initPlugin(Yaf_Dispatcher $dispatcher)
    {
        // 注册一个插件
        // $objSamplePlugin = new SamplePlugin();
        // $dispatcher->registerPlugin($objSamplePlugin);
    }

    public function _initRoute(Yaf_Dispatcher $dispatcher)
    {
        // 在这里注册自己的路由协议,默认使用简单路由
    }
	public function _initView(Yaf_Dispatcher $dispatcher)
    {
        // 在这里注册自己的view控制器，例如smarty,firekylin
        if (REQUEST_METHOD != 'CLI') {
            $smarty = new SmartyAdapter(null, $this->config->smarty);
            // $smarty->registerFunction('function','checkRight',array());
            $dispatcher->setView($smarty);
        }
    }
}