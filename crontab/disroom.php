<?php
require('/www/web/majiang/public_html/server/crontab.php');
require('/www/web/majiang/public_html/server/library/db_mysql.inc.php');
$config_obj=Yaf_Registry::get("config");
$config_database =$config_obj->database->config->toArray();
$host =$config_database['host'];
$user =$config_database['user'];
$password =$config_database['pwd'];
$database =$config_database['name'];

$db = new DB_Sql($host, $user, $password, $database);
$redis = RedisDB::factory('game');
$redisUser =RedisDB::factory('user');
$userlist = $this->redis->hGetAll('userlist');

$list = $redis->hGetAll("roomlist");
$expired_roomid =array();
if(is_array($list)&&$list){
	foreach ($list as $roomid => $value) {
		//20分钟未开始游戏 房间自动解散
		$room = $redis->hGetAll("room:".$roomid);
		$create = $room['time'];
		if(!$room['pan'] && !$room['state'] && time()>$create+1200){
			//过期解散
			$expired_roomid[]=$roomid;
			$log=new LogModel($roomid);
			$log->getUsers();
			//删除房间
			$redis->del("room:".$roomid);
			$redis->del("roomlist",$roomid);
		}

	}
}
//清理过期房间占用座位
if(is_array($userlist) && $userlist){
	foreach($userlist as $cur_uid=>$user){
		$userInfo =json_decode($user,true);
		$uid =$userInfo['uid'];
		if(isset($userInfo['seatid']) && in_array($userInfo['seatid'],$expired_roomid)){
				$mUser = new UserModel($uid);
				$user_info=$mUser->getInfo();
				$user_info['roomid']='';
				$mUser->save($user_info);
		}
	}
}
