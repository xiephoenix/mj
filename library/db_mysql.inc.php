<?php
class DB_Sql {
  
  /* public: connection parameters */
  
  public $Host     = "";
  public $Database = "";
  public $User     = "";
  public $Password = "";  

  /* public: configuration parameters */
  public $Auto_Free     = 0;     ## Set to 1 for automatic mysql_free_result()
  public $Debug         = 0;     ## Set to 1 for debugging messages.
  public $Halt_On_Error = "yes"; ## "yes" (halt with message), "no" (ignore errors quietly), "report" (ignore errror, but spit a warning)
  public $Seq_Table     = "db_sequence";

  /* public: result array and current row number */
  public $Record   = array();
  public $Row;

  /* public: current error number and error text */
  public $Errno    = 0;
  public $Error    = "";

  /* public: this is an api revision, not a CVS revision. */
  public $type     = "mysql";
  public $revision = "4.0";

  /* private: link and query handles */
  public $Link_ID  = 0;
  public $Query_ID = 0;
 

  /* public: constructor */
  public function __construct($host, $user, $password, $database) {
      $this->Host = $host;
      $this->User = $user;
      $this->Password = $password;
      $this->Database = $database;
      
      $this->connect();
  }

  /* public: some trivial reporting */
  function link_id() {
    return $this->Link_ID;
  }

  function query_id() {
    return $this->Query_ID;
  }

  /* public: connection management */
  function connect($Database = "", $Host = "", $User = "", $Password = "") {
    /* Handle defaults */
    if ("" == $Database)
      $Database = $this->Database;
    if ("" == $Host)
      $Host     = $this->Host;
    if ("" == $User)
      $User     = $this->User;
    if ("" == $Password)
      $Password = $this->Password;
      
    /* establish connection, select database */
    if ( 0 == $this->Link_ID ) {
    
      $this->Link_ID=mysqli_connect($Host, $User, $Password,$Database);
      if (!$this->Link_ID) {
        $this->halt("connect failed.");
        return 0;
      }

      // if (!@mysql_select_db($Database,$this->Link_ID)) {
      //   $this->halt("数据库不存在");
      //   return 0;
      // }
     mysqli_query( $this->Link_ID, 'SET NAMES "utf8" COLLATE "utf8_general_ci"' );
	  }
    return $this->Link_ID;

  }

  /* public: discard the query result */
  function free() {
      @mysql_free_result($this->Query_ID);
      $this->Query_ID = 0;
  }

  /* public: perform a query */
  function query($Query_String) {
    /* No empty queries, please, since PHP4 chokes on them. */
    if ($Query_String == "")
      /* The empty query string is passed on from the constructor,
       * when calling the class without a query, e.g. in situations
       * like these: '$db = new DB_Sql_Subclass;'
       */
      return 0;

    if (!$this->connect()) {
      return 0; /* we already complained in connect() about that. */
    };
	

    # New query, discard previous result.
    if ($this->Query_ID) {
      $this->free();
    }

    if ($this->Debug)
      printf("Debug: query = %s<br>\n", $Query_String);
    //mysql_query("SET NAMES 'utf8'");
    $this->Query_ID = @mysqli_query($this->Link_ID,$Query_String);
    $this->Row   = 0;
    $this->Errno = mysqli_connect_errno();
    $this->Error = mysqli_connect_errno();
    if (!$this->Query_ID) {
      $this->halt("Invalid SQL: ".$Query_String);
    }

    # Will return nada if it fails. That's fine.
    return $this->Query_ID;
  }

  /* public: walk result set */
  function next_record() {

    if (!$this->Query_ID) {
      $this->halt("next_record called with no query pending.");
      return 0;
    }
	
	
    $this->Record = @mysqli_fetch_array($this->Query_ID);
    $this->Row   += 1;
    $this->Errno  = mysqli_connect_errno();
    $this->Error  = mysqli_connect_errno();

    $stat = is_array($this->Record);
    if (!$stat && $this->Auto_Free) {
      $this->free();
    }
    return $stat;
  }

  /* public: walk result set  */
  function nextRecord($args = 0) {

    if (!$this->Query_ID) {
      $this->halt("next_record called with no query pending.");
      return 0;
    }

	switch($args){
		case 1:
			$query_func = "mysqli_fetch_row";
		break;
		case 2:
			$query_func = "mysqli_fetch_assoc";
		break;
		default :
			$query_func = "mysqli_fetch_array";
	}
	
    $this->Record = @$query_func($this->Query_ID);
    $this->Row   += 1;
    $this->Errno  = mysqli_connect_errno();
    $this->Error  = mysqli_connect_errno();

    $stat = is_array($this->Record);
    if (!$stat && $this->Auto_Free) {
      $this->free();
    }
    return $stat;
  }

	function getRecord(){
		if(is_array($this->Record)){
			return $this->Record;
		}else{
			return false;
		}
	}

  /* public: position in result set */
  function seek($pos = 0) {
    $status = @mysql_data_seek($this->Query_ID, $pos);
    if ($status)
      $this->Row = $pos;
    else {
      $this->halt("seek($pos) failed: result has ".$this->num_rows()." rows");

      /* half assed attempt to save the day, 
       * but do not consider this documented or even
       * desireable behaviour.
       */
      @mysql_data_seek($this->Query_ID, $this->num_rows());
      $this->Row = $this->num_rows;
      return 0;
    }

    return 1;
  }

  /* public: table locking */
  function lock($table, $mode="write") {
    $this->connect();
    
    $query="lock tables ";
    if (is_array($table)) {
      while (list($key,$value)=each($table)) {
        if ($key=="read" && $key!=0) {
          $query.="$value read, ";
        } else {
          $query.="$value $mode, ";
        }
      }
      $query=substr($query,0,-2);
    } else {
      $query.="$table $mode";
    }
    $res = @mysql_query($this->Link_ID,$query);
    if (!$res) {
      $this->halt("lock($table, $mode) failed.");
      return 0;
    }
    return $res;
  }
  
  function unlock() {
    $this->connect();

    $res = @mysql_query("unlock tables");
    if (!$res) {
      $this->halt("unlock() failed.");
      return 0;
    }
    return $res;
  }


  /* public: evaluate the result (size, width) */
  function affected_rows() {
    return @mysql_affected_rows($this->Link_ID);
  }
  function affectedRows() {
    return $this->affected_rows();
  }

  function num_rows() {
    return @mysql_num_rows($this->Query_ID);
  }
  function numRows() {
    return $this->num_rows();
  }

  function num_fields() {
    return @mysql_num_fields($this->Query_ID);
  }

  /* public: shorthand notation */
  function nf() {
    return $this->num_rows();
  }

  function np() {
    print $this->num_rows();
  }

  function f($Name) {
    return $this->Record[$Name];
  }

  function p($Name) {
    print $this->Record[$Name];
  }

  /* public: sequence numbers */
  function nextid($seq_name) {
    $this->connect();
    
    if ($this->lock($this->Seq_Table)) {
      /* get sequence number (locked) and increment */
      $q  = sprintf("select nextid from %s where seq_name = '%s'",
                $this->Seq_Table,
                $seq_name);
      $id  = @mysql_query($q, $this->Link_ID);
      $res = @mysql_fetch_array($id);
      
      /* No current value, make one */
      if (!is_array($res)) {
        $currentid = 0;
        $q = sprintf("insert into %s values('%s', %s)",
                 $this->Seq_Table,
                 $seq_name,
                 $currentid);
        $id = @mysql_query($q, $this->Link_ID);
      } else {
        $currentid = $res["nextid"];
      }
      $nextid = $currentid + 1;
      $q = sprintf("update %s set nextid = '%s' where seq_name = '%s'",
               $this->Seq_Table,
               $nextid,
               $seq_name);
      $id = @mysql_query($q, $this->Link_ID);
      $this->unlock();
    } else {
      $this->halt("cannot lock ".$this->Seq_Table." - has it been created?");
      return 0;
    }
    return $nextid;
  }
  public function fetchRow($sql = ""){
        $row = array();
        //$query = $this->query($sql);
        $con =$this->link_id();
        $query = mysqli_query($con,$sql);
        $row = @mysqli_fetch_array($query, MYSQLI_ASSOC);
        return $row;
    }
  public function fetchAll($sql = ""){
        $result = array();
        //$query = $this->query($sql);
        $con =$this->link_id();
        $query = mysqli_query($con,$sql);
        while($row = mysqli_fetch_assoc($query)){
             $result[] = $row;
        }
        return $result;
    }
  
  /* public: return table metadata */
  function metadata($table='',$full=false) {
    $count = 0;
    $id    = 0;
    $res   = array();

    if ($table) {
      $this->connect();
      $id = @mysql_list_fields($this->Database, $table);
      if (!$id)
        $this->halt("Metadata query failed.");
    } else {
      $id = $this->Query_ID; 
      if (!$id)
        $this->halt("No query specified.");
    }
 
    $count = @mysql_num_fields($id);

    // made this IF due to performance (one if is faster than $count if's)
    if (!$full) {
      for ($i=0; $i<$count; $i++) {
        $res[$i]["table"] = @mysql_field_table ($id, $i);
        $res[$i]["name"]  = @mysql_field_name  ($id, $i);
        $res[$i]["type"]  = @mysql_field_type  ($id, $i);
        $res[$i]["len"]   = @mysql_field_len   ($id, $i);
        $res[$i]["flags"] = @mysql_field_flags ($id, $i);
      }
    } else { // full
      $res["num_fields"]= $count;
    
      for ($i=0; $i<$count; $i++) {
        $res[$i]["table"] = @mysql_field_table ($id, $i);
        $res[$i]["name"]  = @mysql_field_name  ($id, $i);
        $res[$i]["type"]  = @mysql_field_type  ($id, $i);
        $res[$i]["len"]   = @mysql_field_len   ($id, $i);
        $res[$i]["flags"] = @mysql_field_flags ($id, $i);
        $res["meta"][$res[$i]["name"]] = $i;
      }
    }
    
    // free the result only if we were called on a table
    if ($table) @mysql_free_result($id);
    return $res;
  }

  function close(){
	  mysql_close();
  }

  

  /* private: error handling */
  function halt($msg) {
    $this->Error = @mysql_error($this->Link_ID);
    $this->Errno = @mysql_errno($this->Link_ID);
    if ($this->Halt_On_Error == "no")
      return;
    $this->haltmsg($msg);

    if ($this->Halt_On_Error != "report")
      die("Session halted.");
  }

  function haltmsg($msg) {
    printf("</td></tr></table><b>Database error:</b> %s<br>\n", $msg);
    printf("<b>MySQL Error</b>: %s (%s)<br>\n",
      $this->Errno,
      $this->Error);
  }

  function table_names() {
    $this->query("SHOW TABLES");
    $i=0;
    while ($info=mysql_fetch_row($this->Query_ID))
     {
      $return[$i]["table_name"]= $info[0];
      $return[$i]["tablespace_name"]=$this->Database;
      $return[$i]["database"]=$this->Database;
      $i++;
     }
   return $return;
  }

  function insert_id(){
	  return mysql_insert_id();
  }
  function insertId(){
	  return mysqli_insert_id($this->Link_ID);
  }

  function execute($sql,$foreach=0)  {
	$arr = array();
	$result = mysqli_query($this->Link_ID,$sql);
	if ($result)
	{
		while($row = mysqli_fetch_assoc($result))
		{
			$arr[] = $row;
		}
		mysqli_free_result($result);

		if($foreach){
			foreach($arr as $a);
			return $a;
		}
		else{
			return $arr;
		}
	}
	else
	{
		return false;
	}
  }

  function begin(){
	  $this->set_autocommit(0);
	  mysql_query("BEGIN");
  }
  function commit(){
	 mysql_query("COMMIT");	 
	 mysql_query("UNLOCK TABLES");
	 $this->set_autocommit(1);
  }

  //回滚，释放表，设定自动提交 
  function rollback(){
	  mysql_query("ROLLBACK");
	  mysql_query("UNLOCK TABLES");
	  $this->set_autocommit(1);
  }
  function set_autocommit($t){
	  mysql_query("SET AUTOCOMMIT=$t");
  }

}
?>